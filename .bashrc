# start starship
eval "$(starship init bash)"
export PATH="$PATH:$HOME/.cargo/bin"

# vars
export USER="scientiac"
export TERM="linux"

# aliases
alias ll="ls -la"
alias nvim="hx"
