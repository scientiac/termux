# disable lua_ls in lspconfig so that it stops erroring out all the time
return {
  {
    "neovim/nvim-lspconfig",
    opts = {
      servers = {
        lua_ls = false,
      },
    },
  },
}

