{
    programs.nixvim = {
	plugins= {
	    copilot-lua = {
		enable = true;
		panel.enabled = false;
		suggestion.enabled = false;
		filetypes = {
		    "*" = false;
		    python = true;
		    c = true;
		};
	    };
	    copilot-cmp.enable = true;
	};
    };
}
